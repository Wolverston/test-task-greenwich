import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { InternalServerErrorComponent } from "./internal-server-error/internal-server-error.component";
import { CategoriesListComponent } from './categories-list/categories-list.component';
import { SubcategoriesListComponent } from './categories-list/subcategories-list/subcategories-list.component';
import { SubcategoryComponent } from "./categories-list/subcategory/subcategory.component";
import { OnasComponent } from './staticPages/onas/onas.component';
import { FaqComponent } from './staticPages/faq/faq.component';
import { TarifComponent } from './staticPages/tarif/tarif.component';
import { BezopasnostComponent } from './staticPages/bezopasnost/bezopasnost.component';
import { LegalComponent } from './staticPages/legal/legal.component';

const appRoutes: Routes = [
  { path:"", redirectTo: '/main', pathMatch:"full" },
  { path: '404', component: PageNotFoundComponent },
  { path: '500', component: InternalServerErrorComponent },
  
  { path: 'onas', component: OnasComponent },
  { path: 'faq', component: FaqComponent },
  { path: 'tarif', component: TarifComponent },
  { path: 'bezopasnost', component: BezopasnostComponent },
  { path: 'legal', component: LegalComponent },

  { path: 'main', component: CategoriesListComponent, children: [
    { path:":id", component: SubcategoriesListComponent, children: [
        {path: ":id", component: SubcategoryComponent, runGuardsAndResolvers: 'always'}
    ], runGuardsAndResolvers: 'always' },
  ] },
  { path: '**', redirectTo: '/404' },
  
]

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {onSameUrlNavigation: 'reload'}),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
 
}