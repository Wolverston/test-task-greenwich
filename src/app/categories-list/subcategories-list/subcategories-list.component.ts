import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/shared/app.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-subcategories-list',
  templateUrl: './subcategories-list.component.html',
  styleUrls: ['./subcategories-list.component.scss']
})
export class SubcategoriesListComponent implements OnInit {
  public subCategories = [];
  public chosenCategory: string;
  public isSubcategoryChosen;
  constructor(private _appService: AppService, private _router: Router) {}

  ngOnInit() {
    this._appService.getChosenCategories().subscribe(data => {
      this.chosenCategory = data['id'];
      this.subCategories = data['subcategories'];
    });
    this._appService.getIsSubcategoryChosen().subscribe(data => {
      console.log('gotChosen: ', data);
      this.isSubcategoryChosen = data;
    });
  }
  chooseSubcategory(subcategory) {
    this._appService.setChosenSubcategory(subcategory);
    this._appService.setIsSubcategoryChosen(true);
    this._router.navigate(['/main',this.chosenCategory,subcategory.id])
  }
}
