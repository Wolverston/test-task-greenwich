import { Component, OnInit } from '@angular/core';
import { AppService } from '../shared/app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss']
})
export class CategoriesListComponent implements OnInit {
  public categories = [];
  constructor(private _appService: AppService, private _router: Router){}
  ngOnInit() {
    this._appService.getJSON().subscribe(resBody => {
      this.categories = resBody.categories; 
    })  
    
  }
  chooseCategory(category) {
    this._appService.setChosenCategories(category);
    this._appService.setIsSubcategoryChosen(false);
    this._router.navigate(['/main',category.id])
  }
}
