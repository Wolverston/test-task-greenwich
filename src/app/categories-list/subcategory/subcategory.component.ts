import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/shared/app.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-subcategory',
  templateUrl: './subcategory.component.html',
  styleUrls: ['./subcategory.component.scss']
})
export class SubcategoryComponent implements OnInit {
  public isSubcategoryChosen;
  public chosenCategory;
  public chosenSubcategory;
  public formSubmitted: boolean = false;
  myform: FormGroup;
  field1;
  field2;
  constructor(private fb: FormBuilder, private _appService: AppService, private _router: Router, private route: ActivatedRoute) {}


  ngOnInit() {
    this.myform = this.fb.group({
      field1: ['', Validators.compose([Validators.required])],
      field2: ['', Validators.compose([Validators.required])]
    });
    this._appService.getChosenCategories().subscribe(data => {
      console.log('gotChosen: ', data);
      this.chosenCategory = data['name'];
    });
    this._appService.getChosenSubcategory().subscribe(data => {
      console.log('gotChosen: ', data);
      this.chosenSubcategory = data['name'];
    });
    this._appService.getIsSubcategoryChosen().subscribe(data => {
      this.isSubcategoryChosen = data;
    });
  }
  hideInfo(){
    this.formSubmitted = false;
  }
  onFormSubmit(myform){ 
    console.log(myform.value); 
    this.field1 = myform.value.field1;
    this.field2 = myform.value.field2;
     this.formSubmitted = true; };
  back(){
    this._appService.setIsSubcategoryChosen(false);
    this._router.navigate(['../'], {relativeTo: this.route})
  }
}
