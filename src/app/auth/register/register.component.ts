import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { equalValueValidator } from '../../shared/equal-value-validator';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  isModalOpen: boolean = false;
  constructor(private fb: FormBuilder){}

  ngOnInit() {
      this.registerForm = this.fb.group({
      'field1': ['', Validators.compose([Validators.required])],
      'field2': ['', Validators.compose([Validators.required])],
      'field3': ['', Validators.compose([Validators.required])],
      },{validator: equalValueValidator('field2', 'field3')} );
  }

  toggleModal(){
    this.isModalOpen = !this.isModalOpen;
  }
  onFormSubmit(registerForm){ console.log(registerForm.value) };

}
