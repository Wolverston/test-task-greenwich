import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isModalOpen: boolean = false;
  constructor(private fb: FormBuilder){}

  ngOnInit() {
      this.loginForm = this.fb.group({
      field1: ['', Validators.compose([Validators.required])],
      field2: ['', Validators.compose([Validators.required])]
      });
  }
  toggleModal(){
    this.isModalOpen = !this.isModalOpen;
  }
  onFormSubmit(loginForm){ console.log(loginForm.value) };
}
