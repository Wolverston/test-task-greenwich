
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpModule } from '@angular/http';

import { HttpClientModule } from '@angular/common/http';
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { InternalServerErrorComponent } from './internal-server-error/internal-server-error.component';
import { HeaderComponent } from './header/header.component';
import { CategoriesListComponent } from './categories-list/categories-list.component';
import { SubcategoryComponent } from './categories-list/subcategory/subcategory.component';
import { SubcategoriesListComponent } from './categories-list/subcategories-list/subcategories-list.component';
import { OnasComponent } from './staticPages/onas/onas.component';
import { FaqComponent } from './staticPages/faq/faq.component';
import { TarifComponent } from './staticPages/tarif/tarif.component';
import { BezopasnostComponent } from './staticPages/bezopasnost/bezopasnost.component';
import { LegalComponent } from './staticPages/legal/legal.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';



@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    InternalServerErrorComponent,
    HeaderComponent,
    CategoriesListComponent,
    SubcategoryComponent,
    SubcategoriesListComponent,
    OnasComponent,
    FaqComponent,
    TarifComponent,
    BezopasnostComponent,
    LegalComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
