import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';
import * as Rx from 'rxjs';
@Injectable({
providedIn: 'root',
})

export class AppService {
private chosenCategories = new Rx.ReplaySubject();
setChosenCategories(val) { this.chosenCategories.next(val); }
getChosenCategories(){ return this.chosenCategories; }

private chosenSubcategory = new Rx.ReplaySubject();
setChosenSubcategory(val){ this.chosenSubcategory.next(val); }
getChosenSubcategory(){ return this.chosenSubcategory; }

private isSubcategoryChosen = new Rx.ReplaySubject();
setIsSubcategoryChosen(val) { this.isSubcategoryChosen.next(val); }
getIsSubcategoryChosen(){ return this.isSubcategoryChosen; }



constructor(private _http: HttpClient) {}
  
public getJSON(): Observable<any> {
  return this._http.get("/assets/serverData.json")
}  
}