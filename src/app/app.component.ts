import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'Test App - Valeshkovsky K.';
  constructor(private _router: Router){}

  ngOnInit(){
    this._router.navigate(['/']);
  }
}
